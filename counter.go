package counter

import (
	"bufio"
	"io"
	"sort"
	"strings"
	"unicode"
)

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// Word structure contains word itself and frequency
type Word struct {
	Word  string
	Count int
}

// trim all the "not the letter" runes and convert to lowercase
func trim(token string) string {
	return strings.TrimFunc(token, func(r rune) bool {
		return !unicode.IsLetter(r)
	})
}

func dropStopWords(token string) string {
	if _, ok := stopWords[token]; ok {
		return ""
	}
	return token
}

// collect words in dictionary
func buildMap(r io.Reader) map[string]int {
	words := make(map[string]int)
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		token := trim(scanner.Text())
		token = dropStopWords(token)
		if token == "" {
			continue
		}
		words[strings.ToLower(token)]++
	}
	return words
}

// CountWords unpack dictionary into slice and return subslice of most frequently used words
func CountWords(r io.Reader, top int) []Word {
	words := buildMap(r)
	result := make([]Word, 0, len(words))
	for w, freq := range words {
		result = append(result, Word{
			Word:  w,
			Count: freq,
		})
	}
	sort.Slice(result, func(i, j int) bool {
		if result[i].Count == result[j].Count {
			// compare lexicograpically and sort ascending
			return result[i].Word < result[j].Word
		}
		// sort descending
		return result[i].Count > result[j].Count
	})
	return result[0:min(top, len(result))]
}

// CountWordsInString - user friendly wrapper over CountWords
func CountWordsInString(text string, top int) []Word {
	return CountWords(strings.NewReader(text), top)
}
